import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Layouts/Header';
import Footer from './Layouts/Footer';
import Home from './Layouts/Home';
import CategoryCreate from './Category/create';
import CategoryView from './Category/view';
import CategoryEdit from './Category/edit';

class Index extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="">
                    <Header />

                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path="/create" component={CategoryCreate} />
                        <Route path="/view" component={CategoryView} />
                        <Route path="/edit/:id" component={CategoryEdit} />
                    </Switch>

                    <Footer />
                </div>
            </BrowserRouter>
        );
    }
}

export default Index;

if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
