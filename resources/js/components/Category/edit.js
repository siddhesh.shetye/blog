import React, { Component } from 'react';
import axios from 'axios';

class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    componentDidMount() {
        axios.get('http://localhost:8000/category/edit/'+this.props.match.params.id)
        .then(response => {
            console.log(response);
            this.setState({
                name: response.data.name,
                isLoaded: true,
            })
        });
    }

    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSubmit(event) {
        event.preventDefault();

        const { history } = this.props;

        const category = {
            name: this.state.name
        }

        axios.put('http://localhost:8000/category/update/'+this.props.match.params.id, category)
            .then(response => {

                // redirect to the view page
                history.push('/view')
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
      }

      renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
          return (
            <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
          )
        }
      }

    render() {
        return (
            <div className="uk-section uk-section-primary uk-preserve-color">
                <div className="uk-container">

                    <div className="uk-panel uk-light uk-margin-medium">
                        <h3> Add Category </h3>
                    </div>

                    <form onSubmit={this.onSubmit}>
                        <div className="uk-margin">
                            <input
                                id='name'
                                type='text'
                                name='name'
                                value={this.state.name}
                                onChange={this.handleFieldChange}
                                className={`uk-input uk-form-success ${this.hasErrorFor('name') ? 'is-invalid' : ''}`}
                                placeholder="category name" />
                        </div>

                        <div>
                            <button type="submit" className="uk-button uk-button-danger">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        );
    }
}

export default Edit;