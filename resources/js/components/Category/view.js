import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'

class View extends Component {

    constructor() {
        super();
        this.state = {
            categories: {},
            isLoaded: false
        }
    }

    componentDidMount() {
        this.getCategoryData();
    }

    getCategoryData() {

        axios.get('http://localhost:8000/category')
            .then(response => {

                this.setState({
                    categories: response.data.category,
                    isLoaded: true,
                })
            });
    }

    onDestroy(categoryid) {

        axios.delete('http://localhost:8000/category/destroy/' + categoryid)
            .then(response => {
                console.log(response.data.message);
            })
    }

    render() {
        const { categories, isLoaded } = this.state;
		console.log(this.state);

        if (!isLoaded) {
            return <div>Loading...</div>
        }
        else {
            return (
                <div className="uk-section uk-section-primary uk-preserve-color">
                    <div className="uk-container">

                        <div className="uk-panel uk-light uk-margin-medium">
                            <h3> View Category </h3>
                        </div>

                        <table className="uk-table uk-table-hover uk-table-divider">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category name</th>
                                    <th>Category status</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    categories.map(category => {
                                        return (
                                            <tr key={category.id.toString()}>
                                                <td>1</td>
                                                <td>{category.name}</td>
                                                <td>{category.status}</td>
                                                <td>{category.created_at}</td>
                                                <td > 
                                                    <Link className="uk-button uk-button-secondary" to={`/edit/${category.id}`}> Edit </Link>
                                                    <button className="uk-button uk-button-danger" onClick={this.onDestroy.bind(this, category.id)} > Delete </button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>

                    </div>
                </div>
            );
        }
    }
}

export default View;