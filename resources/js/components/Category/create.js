import React, { Component } from 'react';
import axios from 'axios';

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewCategory = this.handleCreateNewCategory.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleCreateNewCategory(event) {
        event.preventDefault();

        const { history } = this.props;

        const category = {
            name: this.state.name
        }

        axios.post('http://localhost:8000/category/store', category)
            .then(response => {

                // redirect to the view page
                history.push('/view')
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
      }

      renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
          return (
            <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
          )
        }
      }

    render() {
        return (
            <div className="uk-section uk-section-primary uk-preserve-color">
                <div className="uk-container">

                    <div className="uk-panel uk-light uk-margin-medium">
                        <h3> Add Category </h3>
                    </div>

                    <form onSubmit={this.handleCreateNewCategory}>
                        <div className="uk-margin">
                            <input
                                id='name'
                                type='text'
                                name='name'
                                value={this.state.name}
                                onChange={this.handleFieldChange}
                                className={`uk-input uk-form-success ${this.hasErrorFor('name') ? 'is-invalid' : ''}`}
                                placeholder="category name"
                                required />
                        </div>

                        <div>
                            <button type="submit" className="uk-button uk-button-danger">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        );
    }
}

export default Create;