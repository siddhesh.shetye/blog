import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <nav className="uk-navbar-container pr-60 pl-60" uk-navbar="true">
                <div className="uk-navbar-left">
                    <ul className="uk-navbar-nav">
                        <li className="uk-active">
                            <a href="#">
                                <img src="img/logo.png" alt="appointmentlogo" className="height-50" />
                            </a>
                        </li>
                    </ul>
                </div>

                <div className="uk-navbar-right">
                    <ul className="uk-navbar-nav">
                        <li className="uk-active"><a href="#">Active</a></li>
                        <li>
                            <a href="#">Parent</a>
                            <div className="uk-navbar-dropdown">
                                <ul className="uk-nav uk-navbar-dropdown-nav">
                                    <li className="uk-active"><a href="#">Active</a></li>
                                    <li><a href="#">Item</a></li>
                                    <li><a href="#">Item</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">Item</a></li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;