import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from 'react-router-dom';

class Home extends Component {
    render() {
        return (
            <div className="uk-section uk-section-primary uk-preserve-color">
                <div className="uk-container">

                    <div className="uk-panel uk-light uk-margin-medium">
                        <h3> Dashboard </h3>
                    </div>

                    <div className="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid="true">
                        <div>
                            <div className="uk-card uk-card-secondary">
                                <div className="uk-overlay-default uk-position-cover"></div>
                                <div className="uk-card-header">
                                    <h3 className="uk-card-title uk-margin-remove-bottom">  </h3>
                                </div>
                                <div className="uk-card-body uk-text-center">
                                    <img src="img/logo_icon.png" alt="appointmentlogo" />
                                </div>
                                <div className="uk-card-footer">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="uk-card uk-card-default">
                                <div className="uk-card-header">
                                    <h3 className="uk-card-title uk-margin-remove-bottom">Add Category</h3>
                                </div>
                                <div className="uk-card-body uk-text-center">
                                    <img src="img/logo_icon.png" alt="appointmentlogo" />
                                </div>
                                <div className="uk-card-footer">
                                    <Link to="/create" className="uk-button uk-button-text"> View more </Link>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="uk-card uk-card-default">
                                <div className="uk-card-header">
                                    <h3 className="uk-card-title uk-margin-remove-bottom">View Category</h3>
                                </div>
                                <div className="uk-card-body uk-text-center">
                                    <img src="img/logo_icon.png" alt="appointmentlogo" />
                                </div>
                                <div className="uk-card-footer">
                                    <Link to="/view" className="uk-button uk-button-text"> View more </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Home;